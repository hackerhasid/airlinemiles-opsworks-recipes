node[:deploy].each do |app, deploy|
  Chef::Log.info("Creating secrets")
  Chef::Log.info("deploy = #{deploy['secrets'].inspect}")
  Chef::Log.info("node = #{node['secrets'].inspect}")
  Chef::Log.info("node = #{node['website'].inspect}")

  file File.join(deploy[:deploy_to], 'shared', 'config', 'secrets.yml') do
    content YAML.dump(deploy['secrets'])
  end
end